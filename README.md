# mymathserver

Projet pour illustrer les fonctionnalités de Nix.
Voir le [tuto fonctionnel 44](https://nokomprendo.gitlab.io/posts/tuto_fonctionnel_44/2020-01-22-fr-README.html).


## packager une dépendance manquante

- fichier `nix/cpprestsdk.nix`


## packager le projet

- fichiers `nix/mymathserver.nix` et `default.nix`

on peut tout mettre dans `default.nix` mais l'intérêt de `nix/mymathserver.nix` est de pouvoir intégrer le paquet facilement à `nixpkgs`


## environnement virtuel

```
nix-shell
mkdir build
cd build
cmake ..
make
```


## construire et installer le paquet

- depuis le projet en local :

```
nix-env -i f .
```


- en récupérant directement une archive distante :

```
nix-env -if "https://gitlab.com/nokomprendo/mymathserver/-/archive/master/mymathserver-master.tar.gz"
```


## fixer une version

- fichier `nix/release.nix`

- construction :

```
nix-build nix/release.nix
```


## intégration continue

- fichier `.gitlab-ci.yml`


## cache binaire

- créer un dépôt de cache sur <https://cachix.org>

- construire et pusher la release avec `cachix push`

- activer le cache avec `cachix use` (par exemple dans l'intégration continue)


## construire et déployer une image docker

- fichier `nix/docker.nix`

- construire, charger et tester l'image docker :

```
nix-build nix/docker.nix
docker load < result
docker run --rm -it -p 3000:3000 mymathserver:latest
```

- déployer l'image sur Heroku :

```
heroku login
heroku container:login
heroku create nokomprendo-mymathserver
docker tag mymathserver:latest registry.heroku.com/nokomprendo-mymathserver/web
docker push registry.heroku.com/nokomprendo-mymathserver/web
heroku container:release web --app nokomprendo-mymathserver
```

- tester le déploiement <http://nokomprendo-mymathserver.herokuapp.com> :

```
heroku logs --tail
```

## construire et déployer une machine virtuelle

```
nixops create -d myvm nix/virtualbox.nix
nixops deploy -d myvm --force-reboot
```


## personnaliser une dépendance

<https://github.com/NixOS/nixpkgs/blob/master/pkgs/development/libraries/zlib/default.nix>

<https://github.com/NixOS/nixpkgs/blob/master/pkgs/development/libraries/openssl/default.nix>

- modifier une dérivation : `nix/custom0.nix`

- paramétrer une dérivation : `nix/custom1.nix`

- paramétrer une dérivation, version "fonctionnelle" : `nix/custom2.nix`

- installer des versions différentes en même temps :

```
nix-env -i -f nix/custom0.nix
nix-env -i -f nix/custom1.nix
nix-env -i -f nix/custom2.nix
```



