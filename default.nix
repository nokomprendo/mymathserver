{ pkgs ? import <nixpkgs> {} }:

let
  cpprestsdk = pkgs.callPackage ./nix/cpprestsdk.nix {};
  mymathserver = pkgs.callPackage ./nix/mymathserver.nix { inherit cpprestsdk; };

in mymathserver

