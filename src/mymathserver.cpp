#include "mymath.hpp"

#include <cpprest/http_listener.h>
#include <cstdlib>
#include <iostream>
#include <string>

class App : public web::http::experimental::listener::http_listener {
    private:
        void handleGet(web::http::http_request req) {
            const auto & uri = req.relative_uri();
            const auto & path = uri.path();
            const auto splitPath = web::uri::split_path(path);
            std::cout << path << std::endl;

            if (path == "/") {
                std::string text = "Welcome to mymathserver !";
                req.reply(web::http::status_codes::OK, text);
            }
            else if (splitPath.size() == 2 and splitPath[0] == "mul2") {
                try {
                    int x = std::stoi(splitPath[1]);
                    int r = mul2(x);
                    req.reply(web::http::status_codes::OK, std::to_string(r));
                }
                catch (...) {
                    req.reply(web::http::status_codes::BadRequest);
                }
            }
            else {
                req.reply(web::http::status_codes::NotFound);
            }
        }

    public:
        App(std::string url) : web::http::experimental::listener::http_listener(url) {
            support(web::http::methods::GET, 
                    bind(&App::handleGet, this, std::placeholders::_1));
        }
};

int main() {
    const char * portEnv = std::getenv("PORT");
    const std::string port = portEnv == nullptr ? "3000" : portEnv;
    const std::string address = "http://0.0.0.0:" + port;
    App app(address);
    app.open().wait();
    std::cout << "Listening for requests at: " << address << std::endl;
    while (true) {
        std::string line;
        std::getline(std::cin, line);
        if (line == "quit")
            break;
    }
    app.close().wait();
    return 0;
}

