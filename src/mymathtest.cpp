#include <gtest/gtest.h>

#include "mymath.hpp"

TEST(Mymath, mul2_1) {
    ASSERT_EQ(0, mul2(0));
}

TEST(Mymath, mul2_2) {
    ASSERT_EQ(42, mul2(21));
}

TEST(Mymath, mul2_3) {
    ASSERT_EQ(-42, mul2(-21));
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

