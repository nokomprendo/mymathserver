let
  pkgs = import <nixpkgs> {};

  openssl = pkgs.openssl.override { enableSSL2 = true; };

  cpprestsdk = pkgs.callPackage ./cpprestsdk.nix { inherit openssl; };

  mymathserver = pkgs.callPackage ./mymathserver.nix { inherit cpprestsdk; };

in pkgs.stdenv.mkDerivation rec {
  name = "mymathserver-custom1";
  src = ./.;
  buildPhase = "";
  installPhase = ''
    mkdir -p $out/bin
    cp ${mymathserver}/bin/mymathserver $out/bin/${name}
  '';
}

