{ stdenv, cpprestsdk, boost, cmake, gtest, openssl }:

stdenv.mkDerivation {
  name = "mymathserver";
  version = "0.1";
  src = ../.;
  buildInputs = [ boost cmake cpprestsdk gtest openssl ];
}

