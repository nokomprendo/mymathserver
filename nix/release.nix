let
  rev = "1c92cdaf7414261b4a0e0753ca9383137e6fef06";

  pkgs-src = fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/${rev}.tar.gz";
    sha256 = "0d3fqa1aqralj290n007j477av54knc48y1bf1wrzzjic99cykwh";
  };

  pkgs = import pkgs-src {};
  
in pkgs.callPackage ../default.nix {}
  
