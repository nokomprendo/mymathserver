let
  pkgs = import <nixpkgs> {};

  zlib = pkgs.zlib.overrideDerivation (attrs: {
     configureFlags = [ attrs.configureFlags "--zprefix" ];
  });

  cpprestsdk = pkgs.callPackage ./cpprestsdk.nix { inherit zlib; };

  mymathserver = pkgs.callPackage ./mymathserver.nix { inherit cpprestsdk; };

in pkgs.stdenv.mkDerivation rec {
  name = "mymathserver-custom0";
  src = ./.;
  buildPhase = "";
  installPhase = ''
    mkdir -p $out/bin
    cp ${mymathserver}/bin/mymathserver $out/bin/${name}
  '';
}

