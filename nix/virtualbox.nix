{
  network.description = "mynetwork";

  myserver = { config, pkgs, ... }: 

  let
    myapp = import ./release.nix;

  in {
    networking.firewall.allowedTCPPorts = [ 3000 ];

    systemd.services.myservice = {
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      script = "${myapp}/bin/mymathserver";
    };

    deployment = {
      targetEnv = "virtualbox";
      virtualbox = {
        memorySize = 512; 
        vcpu = 1; 
        headless = true;
      };
    };
  };
}

