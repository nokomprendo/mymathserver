let

  config = {
    packageOverrides = pkgs: {
      openssl = pkgs.openssl.override {
        enableSSL2 = true;
      };
    };
  };

  pkgs = import <nixpkgs> { inherit config; };

  mymathserver = pkgs.callPackage ../default.nix {};

in pkgs.stdenv.mkDerivation rec {
  name = "mymathserver-custom2";
  src = ./.;
  buildPhase = "";
  installPhase = ''
    mkdir -p $out/bin
    cp ${mymathserver}/bin/mymathserver $out/bin/${name}
  '';
}
  
