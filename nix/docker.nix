{ pkgs ? import <nixpkgs> {} }:

let

  mymathserver = import ./release.nix;

  entrypoint = pkgs.writeScript "entrypoint.sh" ''
    #!${pkgs.stdenv.shell}
    $@
  '';

in pkgs.dockerTools.buildImage {
  name = "mymathserver";
  tag = "latest";
  config = {
    WorkingDir = "${mymathserver}";
    Entrypoint = [ entrypoint ];
    Cmd = [ "${mymathserver}/bin/mymathserver" ];
  };
}

